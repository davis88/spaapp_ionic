import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { isPresent } from 'ionic-angular/util/util';
import { LearnDetailsPage } from '../learn-details/learn-details';
import { LearnService } from '../../services/learn.service';
import { CategoryModel } from '../../services/learn.model';
import { RemoteServiceProvider } from '../../providers/remote-service/remote-service';
import { BookingModel } from './booking.model';
import { ServiceModel } from './service.model';
import { Storage } from '@ionic/storage';


/**
 * Generated class for the CurrentBookingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-current-booking',
  templateUrl: 'current-booking.html',
})
export class CurrentBookingPage {

  ionViewDidLoad() {
    console.log('ionViewDidLoad CurrentBookingPage');


    this.storage.get('user').then((val) => {
      // console.log('Logged in user = >>> ', val);
    
      var user_id = val.id

        this.rsp.getBookings(user_id)
        .then(data => {
          console.log(data)


          for (var item in data ) {
            let book = new BookingModel()
          
            var _booking = data[item]
            

            book.date = _booking.date
            book.id = _booking.id
            book.staff_id = _booking.staff_id
            book.start_time = _booking.start_time
            book.time_slot = _booking.time_slot
            
            var _service = new ServiceModel()
            _service.id = _booking.service.id
            _service.name = _booking.service.name
            _service.type = _booking.service.type
            _service.duration = _booking.service.duration

            book.service = _service

            book.staff = _booking.staff.profile_url
            book.profile_url = _booking.staff.profile_url

            this.bookings.push(_booking)

              console.log(_booking);
              console.log("DATA>...")
            
          }

        })
        .catch(error => {
          console.log(error)
        })

  });
  
  }

  _query : string = 'all';
  categories : Array<CategoryModel> = new Array<CategoryModel>();

  // bookings : Array<BookingModel> = new Array<BookingModel>();

  bookings : Array<any> = new Array<any>();

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public learnService: LearnService,
    public rsp: RemoteServiceProvider,
    private storage: Storage
  ) {
    let query_param = navParams.get('query');
    this._query = isPresent(query_param) ? query_param : 'all';
  }

  ionViewWillEnter() {

    this.learnService.getFeedCategories()
    .subscribe(data => {
      this.categories = data.categories
    });

    

  }

  openDetails(params) {
    this.navCtrl.push(LearnDetailsPage, params);
  }


}
