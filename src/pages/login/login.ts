import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,MenuController} from 'ionic-angular';
import { RemoteServiceProvider } from '../../providers/remote-service/remote-service';
import { LearnFeedPage } from '../learn-feed/learn-feed';
import { Storage } from '@ionic/storage';
import { AlertController } from 'ionic-angular';



// import { UserProvider, LoginDTO } from '../../providers/user/user';
// import { Storage } from '@ionic/storage';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  email:string;
  password:string;
  
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public rsp: RemoteServiceProvider, 
    public menuCntlr: MenuController,
    private storage: Storage,
    private alertCtrl: AlertController
  ) {

      this.menuCntlr.swipeEnable(false);
      
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  login(){

    console.log("Logged in as " + this.email + " and " + this.password)

    this.rsp.login(this.email, this.password)
      .then(data => {
        console.log(data)
        console.log("SUCCESS")
        this.storage.set('user', data);
        this.navCtrl.setRoot(LearnFeedPage, { query: 'home' })
      })
      .catch(error => {
        console.log("error")
        console.log(error)
          let alert = this.alertCtrl.create({
            title: 'Login Failed',
            subTitle: 'Please check your username / Password',
            buttons: ['Dismiss']
          });
          alert.present();
      })

    
      // this.navCtrl.setRoot(LearnFeedPage, { query: 'home' })
  }
}
