import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { isPresent } from 'ionic-angular/util/util';
import { LearnDetailsPage } from '../learn-details/learn-details';
import { LearnService } from '../../services/learn.service';
// import { StaffModel } from '../../services/learn.model';
import { RemoteServiceProvider } from '../../providers/remote-service/remote-service';
import { StaffModel } from './staff.model';


@IonicPage()
@Component({
  selector: 'page-staff',
  templateUrl: 'staff.html',
})

export class StaffPage {

  // staffs:any = []
  
  _query : string = 'all';
  staffs : Array<StaffModel> = new Array<StaffModel>();

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public learnService: LearnService,
    public rsp: RemoteServiceProvider,
  ) {
    let query_param = navParams.get('query');
    this._query = isPresent(query_param) ? query_param : 'all';
  }


  getStaff() {
    this.rsp.getStaffs()
    .then(data => {
      
      for (var item in data ) {
        let staff = new StaffModel()
      
        var _staff = data[item]
        staff.address = _staff.address
        staff.name = _staff.name
        staff.contact = _staff.contact
        staff.title = _staff.name
        staff.background = "#0077ff"
        staff.profile_url = _staff.profile_url
        staff.outlet = _staff.outlet.name
        staff.email = _staff.email

        staff.tags = [{
            "name": "Body Massage"
          },
          {
            "name": "Shuiatsu Massage"
          },
          {
            "name": "Tuina Massage"
          }
        ];

        this.staffs.push(staff)

          console.log(_staff);
          console.log("DATA>...")
        
      }

    });
  }

  ionViewWillEnter() {
    
    this.getStaff()

    this.rsp.getBookings("4")
      .then(data => {
        console.log(data)
      })
      .catch(error => {
        console.log(error)
      })

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StaffPage');
  }


  openDetails(params) {
    this.navCtrl.push(LearnDetailsPage, params);
  }

}

