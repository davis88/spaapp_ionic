import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the LogoutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-logout',
  templateUrl: 'logout.html',
})
export class LogoutPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private storage: Storage) {
    
    // reset local storage..
    this.storage.remove('user')

    this.navCtrl.setRoot(LoginPage, { query: 'login' })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LogoutPage');
  }

}
