import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RequestOptions } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


// import {Http ,Response } from '@angular/http';
// import 'rxjs/add/operator/do';
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/catch';

/*
  Generated class for the RemoteServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RemoteServiceProvider {

  constructor(public http: HttpClient) {
    console.log('Hello RemoteServiceProvider Provider');
  }

  // getApiUrl : string = "http://localhost:3000/";
  getApiUrl : string = "http://ec2-18-221-241-215.us-east-2.compute.amazonaws.com/";
  

  getStaffs() {
    return new Promise(resolve => {
      this.http.get(this.getApiUrl+'api/staffs').subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  getBookings(user_id:string){ 
      console.log("CUSTOMER ID")
      console.log(user_id)
      return new Promise(resolve => {
        this.http.get( this.getApiUrl + `api/bookings/current/all/${user_id}`)
        .subscribe(data => {
          resolve(data);
        }, err => {
          console.log(err);
        });
      });
      
 
  }

  login(email, password) {
    
      var _body = {
        username:email,
        password:password
      }
      let body = JSON.stringify(_body);
      
    return new Promise(resolve => {
      this.http.post( this.getApiUrl + 'api/staffs/login', body, httpOptions)
      .map(data => data)
      .subscribe(data => {
        console.log(data)
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

}