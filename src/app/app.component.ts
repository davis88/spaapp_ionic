import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav, App } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LearnFeedPage } from '../pages/learn-feed/learn-feed';
import { AboutUsPage } from "../pages/about-us/about-us";
import { StaffPage } from '../pages/staff/staff';
import { CurrentBookingPage } from '../pages/current-booking/current-booking';
import { LoginPage } from '../pages/login/login';
import { LogoutPage } from '../pages/logout/logout';

@Component({
  selector: 'app-root',
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  // make LearnFeedPage the root (or first) page
  rootPage: any = LoginPage;
  // rootPage: any = LearnFeedPage;

  pages: Array<{title: string, component: any, params: any}>;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    public menu: MenuController,
    public app: App
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

    this.pages = [
      {
        title: 'Home',
        component: LearnFeedPage,
        params: {
          query: 'home'
        }
      },
      {
        title: 'Current Booking',
        component: CurrentBookingPage,
        params: {
          query: 'cb'
        }
      },
      {
        title: 'New Booking',
        component: LearnFeedPage,
        params: {
          query: 'nb'
        }
      },
      {
        title: 'Profile',
        component: StaffPage,
        params: {
          query: 'profile'
        }
      },
      {
        title: 'About us',
        component: AboutUsPage,
        params: {
          query: 'about-us'
        }
      },
      {
        title: 'Log out',
        component: LogoutPage,
        params: {
          query: 'Log out'
        }
      }
      
    ];
  }

  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    this.nav.setRoot(page.component, page.params);
  }
}
