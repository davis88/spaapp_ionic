import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';

import { MyApp } from './app.component';

import { LearnFeedPage } from '../pages/learn-feed/learn-feed';
import { LearnDetailsPage } from '../pages/learn-details/learn-details';
import { QuestionDetailsPage } from '../pages/question-details/question-details';
import { ManageQuestionPage } from '../pages/manage-question/manage-question';
import { ManageAnswerPage } from '../pages/manage-answer/manage-answer';

import { QuestionService } from '../services/question.service';
import { AnswerService } from '../services/answer.service';
import { LearnService } from '../services/learn.service';

import { BrowserModule } from '@angular/platform-browser';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SDKBrowserModule } from '../../sdk/index';
import { AboutUsPageModule } from '../pages/about-us/about-us.module';
import { StaffPageModule } from '../pages/staff/staff.module'
import { RemoteServiceProvider } from '../providers/remote-service/remote-service';
import { HttpModule } from '@angular/http';
import { HttpClient } from '@angular/common/http';

import { HttpClientModule } from '@angular/common/http';
import { CurrentBookingPage } from '../pages/current-booking/current-booking';
import { CurrentBookingPageModule } from '../pages/current-booking/current-booking.module';
import { LoginPageModule } from '../pages/login/login.module';
import { LogoutPageModule } from '../pages/logout/logout.module';
import { IonicStorageModule } from '@ionic/storage';



@NgModule({
  declarations: [
    MyApp,
    LearnFeedPage,
    LearnDetailsPage,
    QuestionDetailsPage,
    ManageQuestionPage,
    ManageAnswerPage
  ],
  imports: [
    AboutUsPageModule,
    StaffPageModule,
    CurrentBookingPageModule,
    LoginPageModule,
    LogoutPageModule,
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),    
    SDKBrowserModule.forRoot(),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LearnFeedPage,
    LearnDetailsPage,
    QuestionDetailsPage,
    ManageQuestionPage,
    ManageAnswerPage,
    CurrentBookingPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    QuestionService,
    AnswerService,
    LearnService,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    RemoteServiceProvider,
    HttpClient
  ]
})
export class AppModule {}
